#!/usr/bin/env python3

import logging
import os
import shlex
import signal
import subprocess
import threading
import time
import sys

import inotify.adapters
from inotify import constants as ic

git_sync_script = os.path.abspath(os.path.join(os.path.dirname(os.path.abspath(__file__)), "../git-sync/git-sync"))
inotify_can_sync = True
timer_can_sync = True
stop = False
stop_main = False


def inotify_thread():
	global git_sync_script
	global inotify_can_sync
	global timer_can_sync
	global stop

	log = logging.getLogger("local_check")

	# Set up notify tree on the current directory
	i = inotify.adapters.InotifyTree(".",
									 mask=(ic.IN_MODIFY |
										   ic.IN_MOVE |
										   ic.IN_CREATE |
										   ic.IN_DELETE))

	# Check for events
	for event in i.event_gen():
		if stop:
			break

		# If timeout on event, ignore
		if event is None:
			continue

		# Extract everything from the event
		header, type_names, path, filename = event

		# If it's in a .git folder, we don't care
		if ".git" in path:
			continue

		# Ignore if it's within git ignore
		try:
			subprocess.check_output(shlex.split("git check-ignore \"{}\"".format(os.path.join(path, filename))))
			continue
		except subprocess.CalledProcessError:
			pass

		# If we can sync, then sync
		if inotify_can_sync:
			timer_can_sync = False

			try:
				subprocess.check_output(git_sync_script, stderr=subprocess.STDOUT)
			except subprocess.CalledProcessError as e:
				log.error("Error when synchronising. Output is:\n %s", e.output)
				stop = True

			timer_can_sync = True


def timer_thread():
	global timer_can_sync
	global inotify_can_sync
	global stop

	# Get the logger
	log = logging.getLogger("remote_check")

	# Start a timer
	time_start = time.perf_counter()

	while not stop:
		# If the current time is larger than 30 seconds, try and synce
		time_current = time.perf_counter()

		if time_current - time_start > 30:

			if timer_can_sync:
				inotify_can_sync = False

				log.info("Checking Remote")

				try:
					subprocess.check_output(git_sync_script, stderr=subprocess.STDOUT)
				except subprocess.CalledProcessError as e:
					log.error("Error when synchronising. Output is:\n %s", e.output)
					stop = True

				inotify_can_sync = True

			time_start = time.perf_counter()

		time.sleep(1)


def sigint_handle(a, b):
	global in_th
	global tm_th
	global stop
	global stop_main

	stop = True

	print("Closing Gracefully")
	in_th.join()
	tm_th.join()
	print("Threads Finished")

	stop_main = True


def main():
	global in_th
	global tm_th

	# Check if git-sync script available
	if not os.path.exists(git_sync_script):
		print("Unable to launch -- Cannot find git-sync")
		exit(1)

	# Setup Logger
	logging.basicConfig(format="%(asctime)s - %(levelname)s - %(name)s : %(message)s",
						datefmt="%x %X",
						level=logging.INFO)

	# Start Threads
	in_th = threading.Thread(target=inotify_thread)
	in_th.start()

	tm_th = threading.Thread(target=timer_thread)
	tm_th.start()

	# Set Signal Handlers
	signal.signal(signal.SIGINT, sigint_handle)
	signal.signal(signal.SIGTERM, sigint_handle)

	# Stay alive
	while not stop_main:
		time.sleep(5)

		if sys.version_info.minor == 8:
			if not in_th.isAlive() or not tm_th.isAlive():
				break
		else:
			if not in_th.is_alive() or not tm_th.is_alive():
				break


if __name__ == '__main__':
	main()
